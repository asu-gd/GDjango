# HOSTING DJANGO APPLICATION
To start the Django application type `pipenv shell` in your terminal and CD into the `logindemo/` directory.  
You can now run by typing `python manage.py runserver` and it will be hosted locally at `http://127.0.0.1:8000/`.

This Django application runs off of a SQLite server and has an admin account already attatched to it. You can access the default admin account page at `http://127.0.0.1:8000/admin`.

Username: `admin`  
Password: `password`

Exploring this admin page will give you an idea of how the data is collected and the schemas behind the models. Feel free to look around and use this project as a reference!
