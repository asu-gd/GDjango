from django.contrib import admin

from .models import FavoriteLanguage

class LanguageAdmin(admin.ModelAdmin):
    lang_list = ('userkey', 'favLanguage')
# Register your models here.
admin.site.register(FavoriteLanguage, LanguageAdmin)