from django.test import TestCase
from django.urls import reverse

from .models import FavoriteLanguage

# Create your tests here.
class HomeViewTest(TestCase):
    def test_hoempage(self):
        response = self.client.get(reverse("home:index"))
        self.assertEqual(response.status_code, 200)
        try:
            user = FavoriteLanguage.objects.get(userKey=response.user)
            self.assertTemplateUsed("home.html")
        except Exception:
            self.assertTemplateUsed("infoenter.html")