from django.shortcuts import render
from django.http import HttpRequest
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic
from django.contrib.auth.models import User

from .models import FavoriteLanguage

# TODO: NEED TO ADD SQL QUERY WHEN USER ADDS FAVORITE LANGUAGE
def index(request:HttpRequest):
    if request.method == 'POST':
        print(request.POST.get('favLanguageBox', None))
        user = User.objects.get(username=request.user)
        favLanguage = FavoriteLanguage(userKey=user, favLanguage=str(request.POST.get('favLanguageBox', None)))
        favLanguage.save()
    

    if request.user.is_authenticated:
        try:
            user = FavoriteLanguage.objects.get(userKey=request.user)
            context = { 
                "currUser" : user.userKey,
                "favLanguage": user.favLanguage 
                }
            return render(request, "home/home.html", context)
        except Exception:
            user = User.objects.get(username=request.user)
            context = { 
                "currUser" : user.username
                }
            return render(request, "home/infoenter.html", context)
        
    return render(request, "home/home.html")

class SignUpView(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy("login")
    template_name = "registration/signup.html"