from django.db import models
from django.conf import settings

class FavoriteLanguage(models.Model):
    userKey = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    favLanguage = models.CharField(max_length=255)

    def __str__(self):
        return str(self.userKey) or ''